
Hooks.once('init', () => {

    loadTemplates([
        "modules/actorforge/templates/race-tab.html",
        "modules/actorforge/templates/abilities-tab.html",
        "modules/actorforge/templates/background-tab.html",
        "modules/actorforge/templates/class-tab.html"
    ]);
});

Handlebars.registerHelper('if_even', function(conditional, options) {
    if((conditional % 2) === 0) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

const ABILITIES = ['STR','DEX','CON','INT','WIS','CHA'];

const MULTICLASS_PREREQUISITES = {
    'Barbarian': 'STR >= 13',
    'Bard': 'CHA >= 13',
    'Cleric': 'WIS >= 13',
    'Druid': 'WIS >= 13',
    'Fighter': 'STR >= 13 || DEX >= 13',
    'Monk': 'DEX >= 13 && WIS >= 13',
    'Paladin': 'STR >= 13 && CHA >= 13',
    'Ranger': 'DEX >= 13 && WIS >= 13',
    'Rogue': 'DEX >= 13',
    'Sorcerer': 'CHA >=  13',
    'Warlock': 'CHA >= 13',
    'Wizard': 'INT >= 13'
};

/**
 * "Interface" definition for
 */
class DnD5eApi {

    fetchRaces() { throw new Error("Not implemented"); }

    fetchAbilities() { throw new Error("Not implemented"); }

    fetchClasses() { throw new Error("Not implemented"); }

    fetchProficiencies() { throw new Error("Not implemented"); }

    fetch() {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.fetchRaces(),
                this.fetchAbilities(),
                this.fetchClasses()
            ])
            .then(results =>  {
                resolve(results.reduce((m, c) => mergeObject(m, c), {}))
            })
            .catch(reason => reject(reason));
        });
    }
}

class Dnd5eAbility {

    get name() { throw new Error("Not implemented"); }

    get fullName() { throw new Error("Not implemented"); }

    get desc() { throw new Error("Not implemented"); }
}

class Dnd5eBaseRace {

    get name() { throw new Error("Not implemented"); }

    get abilityBonuses() { throw new Error("Not implemented"); }

    get startingProficiencies() { throw new Error("Not implemented"); };

    get languages() { throw new Error("Not implemented"); };

    get traits() { throw new Error("Not implemented"); };

    get abilityBonusesDescription() {
        let description = [];
        let bonuses = this.abilityBonuses;
        ABILITIES.forEach((ability, idx) => {
            if(bonuses[idx] > 0) {
                description.push(`${ability}: +${bonuses[idx]}`);
            }
        });
        return description.join(', ');
    }

    abilityBonus(ability) {
        return this.abilityBonuses[ABILITIES.indexOf(ability)];
    }
}

class Dnd5eRace extends Dnd5eBaseRace {

    get speed() { throw new Error("Not implemented"); }

    get age() { throw new Error("Not implemented"); };

    get alignment() { throw new Error("Not implemented"); };

    get languageDescription() { throw new Error("Not implemented"); };

    get size() { throw new Error("Not implemented"); };

    get sizeDescription() { throw new Error("Not implemented"); };

    get subraces() { throw new Error("Not implemented"); };
}

class Dnd5eSubRace extends Dnd5eBaseRace {

    get desc() { throw new Error("Not implemented"); }
}

class Dnd5eClass {

    get name() { throw new Error("Not implemented"); }

    get hitDie() { throw new Error("Not implemented"); }

    get savingThrows() { throw new Error("Not implemented"); }

    get proficiencies() { throw new Error("Not implemented"); }

    get levels() { throw new Error("Not implemented"); }

    get multiClassPrerequisite() {
        return MULTICLASS_PREREQUISITES[this.name];
    }

    eligible(abl) {
        let check = new Function('STR','DEX','CON','INT','WIS','CHA', `return ${this.multiClassPrerequisite}`);
        return check(abl['STR'], abl['DEX'], abl['CON'], abl['INT'], abl['WIS'], abl['CHA']);
    }
}

class Dnd5eClassLevel {

    get level() { throw new Error("Not implemented"); }

    get abilityScoreBonuses() { throw new Error("Not implemented"); }

    get profBonus() { throw new Error("Not implemented"); }
}

class Dnd5eProficiency {

    get name() { throw new Error("Not implemented"); }

    proficiencies() {}
}

class RestDnD5eApi extends DnD5eApi {

    static URL = 'http://www.dnd5eapi.co';

    static url(relative) {
        return `${RestDnD5eApi.URL}${relative}`;
    }

    fetchRaces() {
        return this.fetchList(`/api/races`, data => new RestDnd5eRace(data), 'races');
    }

    fetchAbilities() {
        return this.fetchList(`/api/ability-scores`, data => new RestDnd5eAbility(data), 'abilities');
    }

    fetchClasses() {
        return this.fetchList(`/api/classes`, data => new RestDnd5eClass(data), 'classes');
    }

    fetchList(url, mapping, name) {
        return new Promise((resolve, reject) => {
            fetch(RestDnD5eApi.url(url))
                .then(response => response.json())
                .then(result =>
                    Promise.all(result.results.map(data => fetch(RestDnD5eApi.url(data.url))))
                        .then(responses =>
                            Promise.all(responses.map(response => response.json()))
                                .then(results => {
                                        let result = {}; result[name] = results.map(mapping);
                                        resolve(result)
                                    }
                                )
                        )
                )
                .catch(reason => reject(reason));
        })
    }
}

class RestDnd5eAbility extends Dnd5eAbility {

    constructor(data) {
        super();
        this.data = data;
    }

    get name() {
        return this.data.name;
    }

    get fullName() {
        return this.data.full_name;
    }

    get desc() {
        return super.desc;
    }
}

class RestDnd5eRace extends Dnd5eRace {

    constructor(data) {
        super();
        this.data = data;
        this.fetchSubraces()
    }

    fetchSubraces() {
        Promise.all(this.data.subraces.map(data => fetch(RestDnD5eApi.url(data.url))))
            .then(responses =>
                Promise.all(responses.map(response => response.json()))
                    .then(results =>  {
                        this._subraces = results.filter(data => data).map(data => new RestDnd5eSubRace(data))
                    })
            );
    }

    get name() {
        return this.data.name;
    }

    get speed() {
        return this.data.speed;
    }

    get abilityBonuses() {
        return this.data.ability_bonuses;
    }

    get age() {
        return this.data.age;
    };

    get alignment() {
        return this.data.alignment;
    };

    get languageDescription() {
        return this.data.language_desc;
    };

    get languages() {
        return this.data.languages;
    };

    get size() {
        return this.data.size;
    };

    get sizeDescription() {
        return this.data.size_description;
    };

    get startingProficiencies() {
        return this.data.starting_proficiencies;
    };

    get subraces() {
        return this._subraces;
    };

    get traits() {
        return this.data.traits;
    };

}

class RestDnd5eSubRace extends Dnd5eSubRace {

    constructor(data) {
        super();
        this.data = data;
    }

    get name() {
        return this.data.name;
    }

    get desc() {
        return this.data.desc;
    }

    get abilityBonuses() {
        return this.data.ability_bonuses;
    }

    get startingProficiencies() {
        return this.data.starting_proficiencies;
    }

    get languages() {
        return this.data.languages;
    };

    get traits() {
        return this.data.racial_traits;
    };

}

class RestDnd5eClass extends Dnd5eClass {

    constructor(data) {
        super();
        this.data = data;
        this.fetchLevels();
    }

    fetchLevels() {
        fetch(RestDnD5eApi.url(this.data.class_levels.url.toLowerCase()))
            .then(response => response.json())
            .then(result => this._levels = result.map(data => new RestDnd5eClassLevel(data)))
            .catch(reason => reject(reason));
    }

    get name() {
        return this.data.name;
    }

    get hitDie() {
        return this.data.hit_die;
    }

    get savingThrows() {
        return this.data.saving_throws.map(saving => saving.name );
    }

    get proficiencies() {
        return this.data.proficiencies.map(prf => prf.name )
    }

    get levels() {
        return this._levels;
    }
}

class RestDnd5eClassLevel extends Dnd5eClassLevel {

    constructor(data) {
        super();
        this.data = data;
    }

    get level() {
        return this.data.level;
    }

    get abilityScoreBonuses() {
        return this.data.ability_score_bonuses;
    }

    get profBonus() {
        return this.prof_bonus;
    }
}

class RestDnD5eProficiency extends Dnd5eProficiency {

    constructor(data) {
        super();
        this._name = data.name;
        this.url = data.url;
    }

    get name() {
        return this._name;
    }
}

Hooks.on('renderDialog', (dialog) => {
    if(dialog.data.title === 'Create New Actor') {

        const forgeBtn = $(
            `<button class="dialog-button forge">
                <i class="ra ra-flat-hammer"></i> 
                &nbsp;Actor Forge
            </button>`
        );

        dialog.element.find('.dialog-button').after(forgeBtn);
        dialog.element.find('.forge')[0].addEventListener('click', evt => {
            Loader.show('Fetch DnD5e Api...');
            this.api = new RestDnD5eApi();
            this.api.fetch().then(data => {
                Loader.hide();
                let forge = new ActorForge(data);
                forge.render(true);
                dialog.close();
            });
        });
    }
});

class Loader {

    static show(message) {
        const loader = $(
            `<div id="actorforge-loader" class="loader-overlay">
                <div class="loader-container">
                    <div class="loader-spinner"></div>
                    <div class="loader-text">${message}</div>
                </div>
            </div>`
        );

        loader.appendTo($('body'));
    }

    static hide() {
        $('#actorforge-loader').remove();
    }
}

class Utils {

    static modRenderer(mod) {
        return mod > 0 ? `+${mod}`: mod;
    }

    static setValue(el, value) {
        el.prop('nodeName') === 'INPUT' ? el.val(value) : el.text(value);
    }

}

/**
 *
 */
class ActorForge extends Application {

    constructor(api) {
        super(api);
        this.tabs = [];
        this.tabs['race'] = new RaceTab(api, this);
        this.tabs['ability'] = new AbilityTab(api, this);
        this.tabs['background'] = new BackgroundTab(api, this);
        this.tabs['class'] = new ClassTab(api, this);
    }

    static get defaultOptions() {
        const options = super.defaultOptions;
        options.template = "modules/actorforge/templates/actorforge.html";
        options.width = 800;
        options.height = 520;
        options.title = "Actor Forge";
        return options;
    }

    activateListeners(html) {
        $('#wizard').steps({
            onFinish: this.onFinish,
            onChange: this.onChange
        });

        Object.values(this.tabs).forEach(tab => tab.activateListeners(html));
    }

    getData() {
        let data = {};
        Object.values(this.tabs).forEach(tab => data = mergeObject(data, tab.getData()));
        return data;
    }

    tab(name) {
        return this.tabs[name];
    }

    onFinish() {
        alert('done.');
    }

    onChange() {
        return true;
    }

}

/**
 *
 */
class RaceTab {

    constructor(api, forge) {
        this.races = api.races;
        this.forge = forge;
        this.selectedSubRace = 'none';
    }

    activateListeners(html) {
        $('button.race-toggle').click(evt => {
            let name = $(evt.currentTarget).attr("data-race");
            this.selectedRace = this.races.find(race => race.name === name);
            Hooks.call('ActorForge.raceSelected', this.selectedRace);
            this.toggleRaceButtons();
            this.updateRaceInfo();
        });
    }

    getData() {
        return {
            races: this.races
        };
    }

    toggleRaceButtons() {
        $('button.race-toggle').each((idx, btn) => {
            if($(btn).attr("data-race") === this.selectedRace.name) {
                $(btn).addClass('selected');
            } else {
                $(btn).removeClass('selected');
            }
        });
    }

    updateRaceInfo() {
        const path = "modules/actorforge/templates/race-data.html";
        renderTemplate(path, this.selectedRace).then(html => {
            $('#race-data').html(html);
            $('#subrace').change(evt => {
                this.selectedSubRace = $('#subrace option:selected').val();
                this.updateSubraceInfo();
            });
        });
    }

    updateSubraceInfo() {
        let subraceBonuses = '';
        if(this.selectedSubRace !== 'none') {
            let race = this.races.find(race => race.name === this.selectedRace.name);
            let data = race.subraces.find(race => race.name === this.selectedSubRace);
            subraceBonuses = data.abilityBonusesDescription;
        }
        $('#subrace-bonuses').html(subraceBonuses);
    }

}

/**
 *
 */
class AbilityTab {

    constructor(api, forge) {
        this.abilities = api.abilities;
        this.forge = forge;
        this.selectedAbilityMode = 'none';
        this.modes = {
            'point': AbilityPointMode,
            'dice': AbilityDiceMode,
            'standard': AbilityStandardMode,
            'manual': AbilityManualMode,
        };
    }

    activateListeners(html) {
        $('button.ability-toggle').click(evt => {
            this.selectedAbilityMode = $(evt.currentTarget).attr("data-ability");
            this.toggleAbilityButtons();
            this.renderAbilityMode();
        });
    }

    toggleAbilityButtons() {
        $('button.ability-toggle').each((idx, btn) => {
            if($(btn).attr("data-ability") === this.selectedAbilityMode) {
                $(btn).addClass('selected');
            } else {
                $(btn).removeClass('selected');
            }
        });
    }

    renderAbilityMode() {
        if(this.currentMode) {
            this.currentMode.destroy();
        }
        this.currentMode = new this.modes[this.selectedAbilityMode](this.abilities, this.forge);
        this.currentMode.render();
    }

    getData() {
        return {};
    }

    get selectedAbilities() {
        return this.currentMode ? this.currentMode.selectedAbilities : [];
    }
}

class BaseAbilityMode {

    constructor(abilities, forge, template, drag) {
        this.abilities = abilities;
        this.forge = forge;
        this.template = `modules/actorforge/templates/${template}`;
        this.drag = drag;
        this.abilityBaseValues = [];
        this.selectedAbilities = [];
        this.abilities.forEach(ability => {
            this.abilityBaseValues[ability.name] = 0;
            this.selectedAbilities[ability.name] = 0;
        });
        Hooks.on('ActorForge.raceSelected', () => this.updateData());
    }

    render() {
        renderTemplate(this.template, { abilities: this.abilities }).then(html => {
            $('#abilities-data').html(html);
            this.afterRender();
            this.updateData();
        });
    }

    afterRender() {
        if(this.drag) {
            this.abilities.forEach(ability => {
                $(`#${ability.name}-ability-base-value`).on({
                    dragstart: evt => { this.onAbilityDragStart(evt); },
                    dragenter: (evt) => { this.onAbilityDragEnter(evt); },
                    dragover: (evt) => { evt.preventDefault(); },
                    dragleave: (evt) => { this.onAbilityDragLeave(evt); },
                    drop: (evt) => { this.onAbilityDrop(evt); },
                    dragend: (evt) => { this.onAbilityDragEnd(evt); }
                });
            });
        }
    }

    onAbilityDragStart(evt) {
        $(evt.currentTarget).parent().parent().addClass('ability-ondrag');
        let ability = $(evt.currentTarget).attr("id").split('-')[0];
        evt.originalEvent.dataTransfer.setData("swapWith", ability);
    }

    onAbilityDragEnter(evt) {
        evt.preventDefault();
        $(evt.currentTarget).parent().parent().addClass('ability-ondragover');
    }

    onAbilityDragLeave(evt) {
        evt.preventDefault();
        $(evt.currentTarget).parent().parent().removeClass('ability-ondragover');
    }

    onAbilityDrop(evt) {
        evt.preventDefault();
        $(evt.currentTarget).parent().parent().removeClass('ability-ondragover');
        let abilityDrag = evt.originalEvent.dataTransfer.getData('swapWith');
        let abilityDrop = $(evt.currentTarget).attr("id").split('-')[0];
        this.swapAbilities(abilityDrag, abilityDrop);
        this.updateData();
    }

    onAbilityDragEnd(evt) {
        $(evt.currentTarget).parent().parent().removeClass('ability-ondrag');
    }

    swapAbilities(ability1, ability2) {
        let score1 = this.abilityBaseValues[ability1];
        this.abilityBaseValues[ability1] = this.abilityBaseValues[ability2];
        this.abilityBaseValues[ability2] = score1;
    }

    updateData() {
        let race = this.forge.tab('race').selectedRace;
        this.abilities.forEach(ability => {
            let baseValue = this.abilityBaseValues[ability.name];
            let additional = this.additionalValues(ability.name);
            let raceValue = race ? race.abilityBonus(ability.name) : 0;
            let total = baseValue + additional + raceValue;
            let mod = Math.floor((total - 10) / 2);
            this.selectedAbilities[ability.name] = total;

            Utils.setValue($(`#${ability.name}-ability-base-value`), baseValue);
            Utils.setValue($(`#${ability.name}-race-ability-bonus`), Utils.modRenderer(raceValue));
            Utils.setValue($(`#${ability.name}-total-ability-points`), total);
            Utils.setValue($(`#${ability.name}-total-ability-mod`), Utils.modRenderer(mod));
        });
    }

    additionalValues(ability) {
        return 0;
    }

    destroy() {
    }

}

class AbilityPointMode extends BaseAbilityMode {

    constructor(abilities, forge) {
        super(abilities, forge, "abilities-points-mode.html");
        this.abilityPoints = [];
        this.abilities.forEach(ability => {
            this.abilityBaseValues[ability.name] = 8;
            this.abilityPoints[ability.name] = 0;
        });
    }

    afterRender() {
        super.afterRender();
        $('.ability-point-button').click(evt => {
            let id = $(evt.currentTarget).attr("id");
            let ability = id.split('-')[0];
            let operation = id.split('-')[3];
            let amount = operation === 'sub' ? -1 : 1;
            this.abilityPoints[ability] = this.abilityPoints[ability] + amount;
            this.updateData();
        });
    }

    updateData() {
        super.updateData();
        this.totalPoints = 27 - Object.values(this.abilityPoints).reduce((total, current) => total + current, 0);
        $('#ability-total-points').text(this.totalPoints);
        this.abilities.forEach(ability => {
            let points = this.abilityPoints[ability.name];
            $(`#${ability.name}-ability-points`).text(points);
            $(`#${ability.name}-ability-point-sub`).attr("disabled", points === 0);
            $(`#${ability.name}-ability-point-add`).attr("disabled", this.totalPoints === 0);
        });
    }

    additionalValues(ability) {
        return this.abilityPoints[ability];
    }
}

class AbilityDiceMode extends BaseAbilityMode {

    constructor(abilities, forge) {
        super(abilities, forge, "abilities-dice-mode.html", true);
    }

    afterRender() {
        super.afterRender();
        $('#ability-roll-button').click(evt => {
            this.rollForAbilities();
            this.updateData();
        });
    }

    rollForAbilities() {
        this.abilities.forEach(ability => {
            this.abilityBaseValues[ability.name] = new Roll('4d6kh3').roll().total;
        });
    }
}

class AbilityStandardMode extends BaseAbilityMode {

    constructor(abilities, forge) {
        super(abilities, forge, "abilities-standard-mode.html", true);
        let standardValues = [15,14,13,12,10,8];
        this.abilities.forEach((ability, idx) => {
            this.abilityBaseValues[ability.name] = standardValues[idx];
        });
    }
}

class AbilityManualMode extends BaseAbilityMode {

    constructor(abilities, forge) {
        super(abilities, forge, "abilities-manual-mode.html", false);
        let standardValues = [15,14,13,12,10,8];
        this.abilities.forEach((ability, idx) => {
            this.abilityBaseValues[ability.name] = standardValues[idx];
        });
    }

    afterRender() {
        super.afterRender();
        $('.ability-point-button').click(evt => {
            let id = $(evt.currentTarget).attr("id");
            let ability = id.split('-')[0];
            let operation = id.split('-')[3];
            let amount = operation === 'sub' ? -1 : 1;
            this.abilityBaseValues[ability] = this.abilityBaseValues[ability] + amount;
            this.updateData();
        });
    }
}

class BackgroundTab {

    constructor(api, forge) {
        this.forge = forge;
        this.alignments = {
            'Chaotic Evil': 'Creatures act with arbitrary violence, spurred by their greed, hatred, or bloodlust. Demons, red dragons, and orcs are chaotic evil.',
            'Chaotic Neutral': 'Creatures follow their whims, holding their personal freedom above all else. Many barbarians and rogues, and some bards, are chaotic neutral',
            'Chaotic Good': 'Creatures act as their conscience directs, with little regard for what others expect. Copper dragons, many elves, and unicorns are chaotic good.',
            'Neutral Evil': 'Is the alignment of those who do whatever they can get away with, without compassion or qualms. Many drow, some cloud giants, and goblins are neutral evil.',
            'True Neutral': 'Is the alignment of those who prefer to steer clear of moral questions and don\'t take sides, doing what seems best at the time. Lizardfolk, most druids, and many humans are neutral.',
            'Neutral Good': 'Folk do the best they can to help others according to their needs. Many celestials, some cloud giants, and most gnomes are neutral good.',
            'Lawful Evil': 'Creatures methodically take what they want, within the limits of a code of tradition, loyalty, or order. Devils, blue dragons, and hobgoblins are lawful evil.',
            'Lawful Neutral': 'Individuals act in accordance with law, tradition, or personal codes. Many monks and some wizards are lawful neutral.',
            'Lawful Good': 'Creatures can be counted on to do the right thing as expected by society. Gold dragons, paladins, and most dwarves are lawful good.',
        }
    }

    activateListeners(html) {
        $('button.alignment-toggle').click(evt => {
            this.selectedAlignment = $(evt.currentTarget).attr("data-alignment");
            this.toggleAlignmentButtons();
            this.updateAlignmentDescription();
        });
    }

    toggleAlignmentButtons() {
        $('button.alignment-toggle').each((idx, btn) => {
            if($(btn).attr("data-alignment") === this.selectedAlignment) {
                $(btn).addClass('selected');
            } else {
                $(btn).removeClass('selected');
            }
        });
    }

    updateAlignmentDescription() {
        let description = this.alignments[this.selectedAlignment];
        $('#alignment-description').text(description);
    }

    getData() {
        return {
            alignments: Object.keys(this.alignments)
        };
    }
}

class ClassTab {

    constructor(api, forge) {
        this.classes = api.classes;
        this.forge = forge;
        this.selectedClasses = [];
        this.selectedLevels = {};
        this.template = 'modules/actorforge/templates/class-data.html';
    }

    activateListeners(html) {
        $('button.class-toggle').click(evt => {
            let selectedClass = $(evt.currentTarget).attr("data-class");
            if(!this.selectedClasses.includes(selectedClass)) {
                this.selectedClasses.push(selectedClass);
                this.selectedLevels[selectedClass] = this.buildLevels(selectedClass, 1)
            } else {
                this.selectedClasses = this.selectedClasses.filter(clazz => clazz !== selectedClass);
            }
            this.toggleClassButtons();
            this.checkMultiClassPrerequisites();
            this.updateData();
        });
    }

    buildLevels(className, level) {
        let baseHitDie = this.classByName(className).hitDie;
        let con = Math.floor((this.forge.tab('ability').selectedAbilities['CON'] - 10) / 2);
        let levels = [];
        for(let i = 1; i <= level; i++) {
            let hitDie = i === 1 ? parseInt(baseHitDie) : new Roll(`d${baseHitDie}`).roll().total;
            let total = hitDie + con;
            levels.push({
                level: i,
                hitDie: hitDie,
                con:  Utils.modRenderer(con),
                total: total
            });
        }
        return levels;
    }

    toggleClassButtons() {
        $('button.class-toggle').each((idx, btn) => {
            if(this.selectedClasses.includes($(btn).attr("data-class"))) {
                $(btn).addClass('selected');
            } else {
                $(btn).removeClass('selected');
            }
        });
    }

    checkMultiClassPrerequisites() {
        let abilities = this.forge.tab('ability').selectedAbilities;
        this.classes.forEach(clazz => {
            let eligible = this.selectedClasses.length === 0 || clazz.eligible(abilities);
            if(!this.selectedClasses.includes(clazz.name)) {
                $(`#${clazz.name}-class-selection-btn`).attr("disabled", !eligible);
            }
        });
    }

    updateData() {
        let data = {
            selectedClasses: this.selectedClasses.map(
                name => mergeObject(
                    this.classByName(name), {
                    selectedLevels: this.selectedLevels[name],
                    selectedLevel: this.selectedLevels[name].length+1,
                    totalHitDice: this.selectedLevels[name].map(data => data.total).reduce((tot, level) => tot+level, 0)
                })
            )
        };
        renderTemplate(this.template, data).then(html => {
            $('#class-data').html(html);
            $('.class-level').change(evt => {
                let selectedClass = $(evt.currentTarget).attr('data-class-level');
                let level = $(`#${selectedClass}-class-level option:selected`).val();
                this.selectedLevels[selectedClass] = this.buildLevels(selectedClass, level);
                this.updateData();
            });
        });
    }

    classByName(name) {
        return this.classes.filter(clazz => clazz.name === name)[0];
    }

    getData() {
        return {
            classes: this.classes
        };
    }
}