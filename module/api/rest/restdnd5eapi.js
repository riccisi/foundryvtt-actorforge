
import { DnD5eApi } from "../dnd5eapi.js";
import { Dnd5eRace } from "../dnd5eapi.js";
import { Dnd5eAbility } from "../dnd5eapi.js";
import { Dnd5eSubRace } from "../dnd5eapi.js";
import { Dnd5eClass } from "../dnd5eapi.js";
import { Dnd5eClassLevel } from "../dnd5eapi.js";

/**
 *
 */
export class RestDnD5eApi extends DnD5eApi {

    static get API_URL() {
        return 'http://www.dnd5eapi.co';
    }

    static url(relative) {
        return `${RestDnD5eApi.API_URL}${relative}`;
    }

    fetchRaces() {
        return this.fetchList('/api/races', data => new RestDnd5eRace(data), 'races');
    }

    fetchAbilities() {
        return this.fetchList(`/api/ability-scores`, data => new RestDnd5eAbility(data), 'abilities');
    }

    fetchClasses() {
        return this.fetchList('/api/classes', data => new RestDnd5eClass(data), 'classes');
    }

    /* to implement */
    fetchProficiencies() {
        return Promise.resolve({});
    }

    /* to implement */
    fetchSpells() {
        return Promise.resolve({});
    }

    /* to implement */
    fetchEquipments() {
        return Promise.resolve({});
    }

    fetchList(url, mapping, name) {
        return new Promise((resolve, reject) => {
            fetch(RestDnD5eApi.url(url))
                .then(response => response.json())
                .then(result =>
                    Promise.all(result.results.map(data => fetch(RestDnD5eApi.url(data.url))))
                        .then(responses =>
                            Promise.all(responses.map(response => response.json()))
                                .then(results => {
                                        let result = {}; result[name] = results.map(mapping);
                                        resolve(result)
                                    }
                                )
                        )
                )
                .catch(reason => reject(reason));
        })
    }
}

/**
 *
 */
export class RestDnd5eRace extends Dnd5eRace {

    constructor(data) {
        super();
        this.data = data;
        this.fetchSubraces()
    }

    fetchSubraces() {
        Promise.all(this.data.subraces.map(data => fetch(RestDnD5eApi.url(data.url))))
            .then(responses =>
                Promise.all(responses.map(response => response.json()))
                    .then(results =>  {
                        this._subraces = results.filter(data => data).map(data => new RestDnd5eSubRace(data))
                    })
            );
    }

    get name() {
        return this.data.name;
    }

    get speed() {
        return this.data.speed;
    }

    get abilityBonuses() {
        let bonuses = {};
        if(this.data.ability_bonuses) {
            this.data.ability_bonuses.forEach(bonus => {
                bonuses[bonus.name.toLowerCase()] = bonus.bonus;
            });
        }
        return bonuses;
    }

    get age() {
        return this.data.age;
    };

    get alignment() {
        return this.data.alignment;
    };

    get languageDescription() {
        return this.data.language_desc;
    };

    get languages() {
        return this.data.languages;
    };

    get size() {
        return this.data.size;
    };

    get sizeDescription() {
        return this.data.size_description;
    };

    get startingProficiencies() {
        return this.data.starting_proficiencies;
    };

    get subraces() {
        return this._subraces;
    };

    get traits() {
        return this.data.traits;
    };

}

/**
 *
 */
class RestDnd5eSubRace extends Dnd5eSubRace {

    constructor(data) {
        super();
        this.data = data;
    }

    get name() {
        return this.data.name;
    }

    get desc() {
        return this.data.desc;
    }

    get abilityBonuses() {
        return this.data.ability_bonuses;
    }

    get startingProficiencies() {
        return this.data.starting_proficiencies;
    }

    get languages() {
        return this.data.languages;
    };

    get traits() {
        return this.data.racial_traits;
    };

}

class RestDnd5eAbility extends Dnd5eAbility {

    constructor(data) {
        super();
        this.data = data;
    }

    get name() {
        return this.data.name;
    }

    get fullName() {
        return this.data.full_name;
    }

    get desc() {
        return super.desc;
    }
}

/**
 *
 */
class RestDnd5eClass extends Dnd5eClass {

    constructor(data) {
        super();
        this.data = data;
        this.fetchLevels();
    }

    fetchLevels() {
        fetch(RestDnD5eApi.url(this.data.class_levels.url.toLowerCase()))
            .then(response => response.json())
            .then(result => this._levels = result.map(data => new RestDnd5eClassLevel(data)))
            .catch(reason => reject(reason));
    }

    get name() {
        return this.data.name;
    }

    get hitDie() {
        return this.data.hit_die;
    }

    get savingThrows() {
        return this.data.saving_throws.map(saving => saving.name.toLowerCase() );
    }

    get proficiencies() {
        return this.data.proficiencies.map(prf => prf.name )
    }

    get levels() {
        return this._levels;
    }
}

class RestDnd5eClassLevel extends Dnd5eClassLevel {

    constructor(data) {
        super();
        this.data = data;
    }

    get level() {
        return this.data.level;
    }

    get abilityScoreBonuses() {
        return this.data.ability_score_bonuses;
    }

    get profBonus() {
        return this.data.prof_bonus;
    }
}