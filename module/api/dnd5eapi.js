
function unimplemented() {
    throw new Error("Unimplemented method");
}

/**
 * Abstract class
 */
export class DnD5eApi {

    fetch() {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.fetchRaces(),
                this.fetchAbilities(),
                this.fetchClasses(),
                this.fetchProficiencies(),
                this.fetchSpells(),
                this.fetchEquipments()
            ])
            .then(results =>  {
                resolve(results.reduce((m, c) => mergeObject(m, c), {}))
            })
            .catch(reason => reject(reason));
        });
    }

    /**
     * Implementation should returns a promise that resolve with a list of all available races.
     *
     * @returns a list of {Dnd5eRace}.
     */
    fetchRaces() { unimplemented(); }

    /**
     * Implementation should returns a promise that resolve with a list of all available abilities.
     *
     * @returns a list of {Dnd5eRace}.
     */
    fetchAbilities() { unimplemented(); }

    /**
     * Implementation should returns a promise that resolve with a list of all available classes.
     *
     * @returns a list of {Dnd5eClass}.
     */
    fetchClasses() { unimplemented(); }

    /**
     * Implementation should returns a promise that resolve with a list of all available proficiencies.
     *
     * @returns list of {Dnd5eProficiency}.
     */
    fetchProficiencies() { unimplemented(); }

    /**
     * Implementation should returns a promise that resolve with a list of all available spells.
     *
     * @returns list of {Dnd5eSpell}.
     */
    fetchSpells() { unimplemented(); }

    /**
     * Implementation should returns a promise that resolve with a list of all available items
     * (weapon, armors, objects).
     *
     * @returns list of {Dnd5eEquipment}.
     */
    fetchEquipments() { unimplemented(); }
}

/**
 *
 */
export class Dnd5eBaseRace {

    /**
     * The name of the race.
     */
    get name() { unimplemented(); }

    /**
     * List of ability bonuses provided by this race, ex: { "str": 1, "dex": 2 }.
     */
    get abilityBonuses() { unimplemented(); }

    /**
     *
     */
    get startingProficiencies() { unimplemented(); }

    /**
     *
     */
    get languages() { unimplemented(); }

    /**
     *
     */
    get traits() { unimplemented(); }

    /**
     *
     * @returns {string}
     */
    get abilityBonusesDescription() {
        let description = [];
        Object.entries(this.abilityBonuses).forEach(([ability,bonus]) => {
            let name = CONFIG.DND5E.abilities[ability];
            description.push(`${name}: +${bonus}`);
        });
        return description.join(', ');
    }

    abilityBonus(ability) {
        return this.abilityBonuses[ability] ? this.abilityBonuses[ability] : 0;
    }
}

/**
 *
 */
export class Dnd5eAbility {

    get name() { unimplemented(); }

    get fullName() { unimplemented(); }

    get desc() { unimplemented(); }
}

/**
 *
 */
export class Dnd5eRace extends Dnd5eBaseRace {

    /**
     *
     */
    get speed() { unimplemented(); }

    /**
     *
     */
    get age() { unimplemented(); }

    /**
     *
     */
    get alignment() { unimplemented(); }

    /**
     *
     */
    get languageDescription() { unimplemented(); }

    /**
     *
     */
    get size() { unimplemented(); }

    /**
     *
     */
    get sizeDescription() { unimplemented(); }

    /**
     *
     */
    get subraces() { unimplemented(); }
}

/**
 *
 */
export class Dnd5eSubRace extends Dnd5eBaseRace {

    /**
     *
     */
    get desc() { unimplemented(); }
}

/**
 *
 */
export class Dnd5eClass {

    static get MULTICLASS_PREREQUISITES() {
        return {
            'Barbarian': 'STR >= 13',
            'Bard': 'CHA >= 13',
            'Cleric': 'WIS >= 13',
            'Druid': 'WIS >= 13',
            'Fighter': 'STR >= 13 || DEX >= 13',
            'Monk': 'DEX >= 13 && WIS >= 13',
            'Paladin': 'STR >= 13 && CHA >= 13',
            'Ranger': 'DEX >= 13 && WIS >= 13',
            'Rogue': 'DEX >= 13',
            'Sorcerer': 'CHA >=  13',
            'Warlock': 'CHA >= 13',
            'Wizard': 'INT >= 13'
        }
    }

    /**
     *
     */
    get name() { unimplemented(); }

    /**
     *
     */
    get hitDie() { unimplemented(); }

    /**
     *
     */
    get savingThrows() { unimplemented(); }

    /**
     *
     */
    get proficiencies() { unimplemented(); }

    /**
     *
     */
    get levels() { unimplemented(); }

    /**
     *
     * @returns {*}
     */
    get multiClassPrerequisite() {
        return Dnd5eClass.MULTICLASS_PREREQUISITES[this.name];
    }

    /**
     *
     * @param abl
     * @returns {*}
     */
    eligible(abl) {
        let check = new Function('STR','DEX','CON','INT','WIS','CHA', `return ${this.multiClassPrerequisite}`);
        return check(abl['STR'], abl['DEX'], abl['CON'], abl['INT'], abl['WIS'], abl['CHA']);
    }
}

/**
 *
 */
export class Dnd5eClassLevel {

    /**
     *
     */
    get level() { unimplemented(); }

    /**
     *
     */
    get abilityScoreBonuses() { unimplemented(); }

    /**
     *
     */
    get profBonus() { unimplemented(); }
}

/**
 *
 */
export class Dnd5eProficiency {

    /**
     *
     */
    get name() { unimplemented(); }
}